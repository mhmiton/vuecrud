<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get', 'Crud\StudentCn@index')->name('get');
Route::post('/store', 'Crud\StudentCn@store')->name('store');
Route::get('/edit/{id}', 'Crud\StudentCn@edit')->name('edit');
Route::put('/update/{id}', 'Crud\StudentCn@update')->name('update');
Route::delete('/destroy/{id}', 'Crud\StudentCn@destroy')->name('destroy');
