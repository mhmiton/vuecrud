// https://appdividend.com/2018/11/17/vue-laravel-crud-example-tutorial-from-scratch/

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import Index from './components/crud/Index.vue';
import Create from './components/crud/Create.vue';
import Edit from './components/crud/Edit.vue';

const routes = [
    {
        name: 'index',
        path: '/',
        component: Index
    },
    {
        name: 'create',
        path: '/create',
        component: Create
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: Edit
    }
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');