<?php

namespace App\Http\Controllers\Crud;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;

class StudentCn extends Controller
{
    public function index()
    {
        $students = Student::all();
        return response()->json($students);
    }

    public function create()
    {
        return view('crud.create');
    }

    public function store(Request $request)
    {
        $data['name']       = $request->name;
        $data['roll']       = $request->roll;
        $data['reg_no']     = $request->reg_no;

        $create = Student::create($data);
        return response()->json('added');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $student = Student::find($id);
        return response()->json($student);
    }

    public function update(Request $request, $id)
    {
        $data['name']       = $request->name;
        $data['roll']       = $request->roll;
        $data['reg_no']     = $request->reg_no;

        $update = Student::whereId($id)->update($data);
        return response()->json('updated');
    }

    public function destroy($id)
    {
        $destroy = Student::whereId($id)->delete();
        return response()->json('deleted');
    }
}
